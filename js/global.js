/**
 * File Name: global.js
 *
 * Revision History:
 *       Jake Perry, 2021-04-19 : Created
 */
var errors = "";
var postalCodeRegex = /[a-zA-Z][0-9][a-zA-Z] [0-9][a-zA-Z][0-9]/;
var phoneRegex = /^((\(\d{3}\)?)|(\d{3}-))\d{3}-\d{4}/;
var emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
var emptyRegex = /^$/;
var anythingRegex = /^.+$/;
var totalPrice = 0;

function regexValidate(myRegex, myValue, myError) {
    if (!myRegex.test(myValue)) {
        errors += `${myError}<br>`;
    }
}

function validateForm() {
    errors = "";
    document.getElementById('errorsOutput').innerHTML = "";
    var firstName = document.getElementById('txtFirstName').value;
    var lastName = document.getElementById('txtLastName').value;
    var address = document.getElementById('txtAddress').value;
    var city = document.getElementById('txtCity').value;
    var province = document.getElementById('txtProvince').value;
    var postalCode = document.getElementById('txtPostalCode').value;
    var phoneNumber = document.getElementById('txtPhoneNumber').value;
    var email = document.getElementById('txtEmail').value;
    var users = document.getElementById('txtUsers').value;

    firstName = firstName.trim();
    lastName = lastName.trim();
    city = city.trim();
    province = province.trim();
    phoneNumber = phoneNumber.trim();
    email = email.trim();

    regexValidate(anythingRegex, firstName, "First name is required!");
    regexValidate(anythingRegex, lastName, "Last name is required!");
    regexValidate(anythingRegex, address, "Address is required!");
    regexValidate(anythingRegex, city, "City is required!");
    regexValidate(anythingRegex, province, "Province is required!");
    regexValidate(postalCodeRegex, postalCode, "Postal code must be in format A1A 1A1");
    regexValidate(phoneRegex, phoneNumber, "Phone number must be in format 123-123-1234 OR (123)123-1234");
    regexValidate(emailRegex, email, "Must be a proper email!");
    regexValidate(anythingRegex, users, "Number of users is required!");

    if (document.getElementById('day1').checked != true && document.getElementById('day2').checked != true && document.getElementById('both').checked != true) {
        errors += "Number of days must be chosen!";
    } else {
        var days = document.querySelector('input[name="days"]:checked').value;
    }


    //If errors, print errors, else, print "receipt"
    if(errors) {
        document.getElementById('errorsOutput').innerHTML = errors;
    }
    else {
        if(document.getElementById('day1').checked == true) {
            totalPrice = 350;
        } else if (document.getElementById('day2').checked == true) {
            totalPrice = 450;
        } else if (document.getElementById('both').checked == true) {
            totalPrice = 750;
        }
        if(users > 5) {
            totalPrice = totalPrice * 0.90;
        }
        document.getElementById('errorsOutput').innerHTML = "";
        var postBack = `
        <h1>Success! You have registered for:</h1>
        First Name: ${firstName} <br>
        Last Name: ${lastName} <br>
        Address: ${address} <br>
        City: ${city} <br>
        Province: ${province} <br>
        Postal Code: ${postalCode} <br>
        Phone Number: ${phoneNumber} <br>
        Email: ${email} <br>
        Users: ${users} <br>
        Days: ${days} <br>
        Total Price: ${totalPrice} <br>
        `;
        document.getElementById('postBackInfo').innerHTML = postBack;

        localStorage.setItem("First Name", firstName);
        localStorage.setItem("Last Name", lastName);
        localStorage.setItem("Address", address);
        localStorage.setItem("City", city);
        localStorage.setItem("Province", province);
        localStorage.setItem("Postal Code", postalCode);
        localStorage.setItem("Phone Number", phoneNumber);
        localStorage.setItem("Email", email);
        localStorage.setItem("Users", users);
        localStorage.setItem("Days", days);
        localStorage.setItem("Total Price", totalPrice);
    }
    return false;
}