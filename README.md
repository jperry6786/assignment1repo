**How To Install**

Download the index.html, along with the js & css folders, as they hold their respective files that hold the design and functionality of the webpage.
Host it locally using anything you like
Go to your localhost/index.html to view the page

----------------------------------------------------------------------

**How To Use**

Insert all information in the input fields
Select the days you want to register for (price shown on label)
Click the 'new' button
Will display errors if fields were not filled out, but required to be

----------------------------------------------------------------------

**License Choice**

MIT License is short and simple, only requiring preservation of copyright and license notices, which is why I chose it.